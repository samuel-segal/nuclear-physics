package com.example.nuclearphysics;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;

import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Arrays;
import java.util.function.Consumer;

public class MainActivity extends AppCompatActivity {

    protected PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        pdfView = (PDFView) findViewById(R.id.pdfView);
        pdfView.fromAsset("Introductory_Nuclear_Physics.pdf").load();

        FloatingActionButton button = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        button.setOnTouchListener(this::doTouch);
    }

    private boolean doTouch(View view, MotionEvent motionEvent) {
        float pressure = motionEvent.getPressure();
        final float threshold = 0.15f;

        if(pressure>threshold){
            saveCurrentPage();
        }
        else{
            snapToBookmarkedPage();
        }

        return true;
    }


    private void snapToBookmarkedPage(){
        int bookmarkedPage = UserData.getBookmarkedPage(this);
        pdfView.jumpTo(bookmarkedPage);
    }

    private void saveCurrentPage(){
        int currPage = pdfView.getCurrentPage();
        UserData.setBookmarkedPage(this,currPage);
    }
}