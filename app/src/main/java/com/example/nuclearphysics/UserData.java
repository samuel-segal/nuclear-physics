package com.example.nuclearphysics;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class UserData {

    private static final String TAG = "UserData";

    private static final String PAGE_FILE = "page.txt";

    public static int getBookmarkedPage(Context context){
        try {

            FileInputStream fileInputStream = context.openFileInput(PAGE_FILE);
            int a;
            StringBuilder tempStr = new StringBuilder();
            while ((a = fileInputStream.read()) != -1) {
                tempStr.append((char) a);
            }
            fileInputStream.close();

            int page = Integer.parseInt(tempStr.toString());

            Log.i(TAG,"Got bookmarked page at "+page);
            return page;
        } catch (IOException e) {
            Log.e(TAG, "Error in getting bookmarked page",e);
            return 0;
        }
    }

    public static void setBookmarkedPage(Context context, int page){
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(PAGE_FILE, Context.MODE_PRIVATE);
            //TODO Make this no longer be plaintext
            fileOutputStream.write((page+"").getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
            Log.i(TAG,"Set bookmark page successfully to "+page);
        } catch (IOException e) {
            Log.e(TAG, "Error in setting bookmarked page", e);
        }
    }
}
