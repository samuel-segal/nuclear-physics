# Nuclear Physics

A simple Android app that displays an introductory nuclear physics textbook, with light bookmarking functionality implemented. This is not intended to be anything full fledged or production ready, rather an easy means of viewing a textbook.

A PDF version of the textbook is offered publicly by the University of Central Florida.
